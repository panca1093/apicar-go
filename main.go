package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/panca1093/apicar-go/server"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	srv := server.New()
	srv.Start()
}
