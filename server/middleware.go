package server

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

// JWTAuth - function middleware to auth handler
func JWTAuth(key string, next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Request().Header.Get("x-token")
		if len(strings.TrimSpace(token)) == 0 {
			log.Errorf("[ERROR] Token is not valid")
			return c.JSON(http.StatusUnauthorized, "Token is invalid")
		}
		_, err := parseJWTToken(key, token)
		if valErr, ok := err.(*jwt.ValidationError); ok {
			if valErr.Errors&jwt.ValidationErrorMalformed != 0 {
				log.Errorf("[ERROR] token is malformed")
				return c.JSON(http.StatusUnauthorized, "token is malformed")
			}
			if valErr.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				log.Errorf("[ERROR] token expired")
				return c.JSON(http.StatusUnauthorized, "token expired")
			}
		}
		return next(c)
	}
}

func parseJWTToken(key string, tokenStr string) (bool, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if token.Method == jwt.SigningMethodHS256 {
			return []byte(key), nil
		}
		return nil, fmt.Errorf("invalid sign method")
	})
	if err != nil {
		log.Errorf("[ERROR] parse jwt token")
		return false, err
	}
	if token.Valid {
		return true, nil
	}
	log.Info("[INFO] token somehow invalid")
	return false, fmt.Errorf("invalid token")
}
