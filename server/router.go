package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/panca1093/apicar-go/domain/car"
)

func setupRouter(key string, handler *car.Handler) *echo.Echo {
	router := echo.New()
	router.GET("/", func(c echo.Context) error {
		meta := map[string]string{
			"project": "Car Service",
			"status":  "Up",
		}
		return c.JSON(http.StatusOK, meta)
	})
	router.GET("/auth", handler.Authentication)
	router.POST("/newcar", JWTAuth(key, handler.CreateDataCar))
	router.GET("/cars", handler.FetchData)
	router.PUT("/car", JWTAuth(key, handler.UpdateDataCar))
	router.GET("/search", handler.SearchbyName)
	router.GET("/car/:idCar", handler.Detail)
	router.DELETE("/car/:idCar", JWTAuth(key, handler.DeleteData))
	return router
}
