package server

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/panca1093/apicar-go/connection"
	"gitlab.com/panca1093/apicar-go/domain/car"
)

// Server - server struct
type Server struct{}

// New - Function to initiate new server
func New() *Server {
	return &Server{}
}

// Start - Function to start server
func (s *Server) Start() {

	// Setup variable mongodb
	mgoHost := os.Getenv("MONGO_DB_HOST")
	mgoDBName := os.Getenv("MONGO_DB_NAME")
	mgoUsername := os.Getenv("MONGO_DB_USERNAME")
	mgoPassword := os.Getenv("MONGO_DB_PASSWORD")
	// mgoPort := os.Getenv("MONGO_DB_PORT") // use to localhost connection

	// URI to use localhost connection
	// mgoURI := fmt.Sprintf("mongodb://%s:%s@%s:%s/%s", mgoUsername, mgoPassword, mgoHost, mgoPort, mgoDBName)

	// URI to use mongodbAtlas(cloud Mongo) connection
	mgoURI := fmt.Sprintf("mongodb+srv://%s:%s@%s/%s?retryWrites=true&w=majority", mgoUsername, mgoPassword, mgoHost, mgoDBName)

	// Create Connection
	mgoClient, err := connection.NewMongoDBClient(mgoURI)
	if err != nil {
		log.Fatal(err)
	}

	// setup pattern domain
	auth := car.NewAuth()
	repository := car.NewRepo(mgoClient)
	usecase := car.NewUsecase(repository)
	handler := car.New(usecase, auth)

	key := os.Getenv("TEST_SECRET_KEY")
	router := setupRouter(key, handler)
	log.Fatal(router.Start(fmt.Sprintf(":%s", os.Getenv("PORT"))))
}
