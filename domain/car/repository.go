package car

import (
	"context"
	"fmt"
	"strings"

	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Repository - initiate repo struct
type repository struct {
	MongoClient *mongo.Client
}

// Repository - interface repository
type Repository interface {
	Store(ctx context.Context, car *Car) error
	FindAll(ctx context.Context) ([]Car, error)
	FindbyFilter(ctx context.Context, filter Filter) ([]Car, error)
	DeletebyID(ctx context.Context, id string) error
}

// NewRepo - Function new repository
func NewRepo(mongoClient *mongo.Client) Repository {
	return &repository{MongoClient: mongoClient}
}

// Store - Function to store data on db
func (r *repository) Store(ctx context.Context, car *Car) error {
	fmt.Println("Store() is invoke")

	var result Car
	filter := bson.M{"idCar": car.ID}
	coll := collection(r.MongoClient)
	err := coll.FindOneAndUpdate(ctx, filter, bson.M{"$set": &car}, options.FindOneAndUpdate()).
		Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			if err := r.insert(ctx, *car); err != nil {
				log.Errorf("[ERROR] store data: %v", err)
				return err
			}
			return nil
		}
		log.Errorf("[ERROR] store data: %v", err)
		return err
	}
	return nil
}

func (r *repository) FindAll(ctx context.Context) ([]Car, error) {
	fmt.Println("FindAll() is invoke")

	cars := []Car{}
	opts := options.Find()
	filter := bson.M{}
	coll := collection(r.MongoClient)
	cur, err := coll.Find(ctx, filter, opts)
	if err != nil {
		log.Errorf("[ERROR] find all data: %v", err)
		return nil, err
	}
	for cur.Next(ctx) {
		var car Car
		if err := cur.Decode(&car); err != nil {
			return nil, err
		}
		cars = append(cars, car)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	cur.Close(ctx)

	return cars, nil
}

func (r *repository) FindbyFilter(ctx context.Context, filter Filter) ([]Car, error) {
	fmt.Println("FindbyFilter() is invoke")

	if filter.isEmpty() {
		return nil, fmt.Errorf("filter is empty")
	}
	cars := []Car{}
	fil := r.filterClause(ctx, filter)
	coll := collection(r.MongoClient)
	cur, err := coll.Find(ctx, fil, options.Find())
	if err != nil {
		log.Errorf("[ERROR] find data by name: %v", err)
		return nil, err
	}
	for cur.Next(ctx) {
		var car Car
		if err := cur.Decode(&car); err != nil {
			return nil, err
		}
		cars = append(cars, car)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	cur.Close(ctx)

	return cars, nil
}

func (r *repository) DeletebyID(ctx context.Context, id string) error {
	fmt.Println("DeletebyID() is invoke")

	coll := collection(r.MongoClient)
	var document bson.M
	err := coll.FindOneAndDelete(ctx, bson.M{"idCar": id}, options.FindOneAndDelete()).
		Decode(&document)
	if err != nil {
		log.Errorf("[ERROR] delete data: %v", err)
		return err
	}

	log.Info(document)
	return nil
}

func (r *repository) insert(ctx context.Context, car Car) error {
	fmt.Println("insert() is invoke")

	coll := collection(r.MongoClient)
	result, err := coll.InsertOne(ctx, car)
	if err != nil {
		log.Errorf("[ERROR] insert data failed: %v", err)
		return err
	}
	log.Info(result)
	return nil
}

func (r *repository) filterClause(ctx context.Context, filter Filter) bson.M {
	if len(strings.TrimSpace(filter.ID)) != 0 && len(strings.TrimSpace(filter.Name)) != 0 {
		return bson.M{}
	}
	if len(strings.TrimSpace(filter.ID)) != 0 {
		return bson.M{"idCar": filter.ID}
	}
	if len(strings.TrimSpace(filter.Name)) != 0 {
		return bson.M{"name": filter.Name}
	}
	return bson.M{}
}

func collection(client *mongo.Client) *mongo.Collection {
	return client.Database("carsBlueBird").Collection("cars")
}
