package car

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

// Handler - initiate struct data
type Handler struct {
	Usecase Usecase
	Auth    Auth
}

// New - initiate function handler
func New(usecase Usecase, auth Auth) *Handler {
	return &Handler{Usecase: usecase, Auth: auth}
}

// CreateDataCar - Function Handler to create data car
func (h *Handler) CreateDataCar(c echo.Context) error {
	ctx := c.Request().Context()
	car := Car{}
	if err := c.Bind(&car); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err := h.Usecase.ProceedData(ctx, &car)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	data := map[string]interface{}{
		"data": car,
	}
	return c.JSON(http.StatusCreated, data)
}

// FetchData - Function handler to fetch all data
func (h *Handler) FetchData(c echo.Context) error {
	ctx := c.Request().Context()

	cars, err := h.Usecase.FindAll(ctx)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	data := map[string]interface{}{
		"data": cars,
	}
	return c.JSON(http.StatusOK, data)
}

// UpdateDataCar - Function handler to update data car
func (h *Handler) UpdateDataCar(c echo.Context) error {
	ctx := c.Request().Context()
	id := c.QueryParam("id")
	car := Car{}
	if err := c.Bind(&car); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	if len(strings.TrimSpace(id)) != 0 {
		car.ID = id
	}
	err := h.Usecase.ProceedData(ctx, &car)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	data := map[string]interface{}{
		"data": car,
	}
	return c.JSON(http.StatusOK, data)
}

// SearchbyName - Function handler to search data car by name
func (h *Handler) SearchbyName(c echo.Context) error {
	ctx := c.Request().Context()
	name := c.QueryParam("name")
	filter := Filter{
		Name: name,
	}
	cars, err := h.Usecase.FindbyFilter(ctx, filter)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	data := map[string]interface{}{
		"data": cars,
	}
	return c.JSON(http.StatusOK, data)
}

// Detail - Function handler to detail data car
func (h *Handler) Detail(c echo.Context) error {
	ctx := c.Request().Context()
	id := c.Param("idCar")
	filter := Filter{
		ID: id,
	}
	cars, err := h.Usecase.FindbyFilter(ctx, filter)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	data := map[string]interface{}{
		"data": cars,
	}
	return c.JSON(http.StatusOK, data)
}

// DeleteData - Function handler to delete data car
func (h *Handler) DeleteData(c echo.Context) error {
	ctx := c.Request().Context()
	id := c.Param("idCar")
	if len(strings.TrimSpace(id)) == 0 {
		return c.JSON(http.StatusBadRequest, "delete data failed")
	}
	if err := h.Usecase.DeleteData(ctx, id); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, "delete data failed")
	}
	return c.JSON(http.StatusOK, "success delete data")
}

// Authentication - Function handler to authenticating user
func (h *Handler) Authentication(c echo.Context) error {
	key := c.Request().Header.Get("x-key")
	if len(strings.TrimSpace(key)) == 0 {
		return c.JSON(http.StatusBadRequest, "delete data failed")
	}
	if check := h.Auth.ValidateSecretKey(key); !check {
		return c.JSON(http.StatusUnauthorized, "key is invalid")
	}
	userAuth, err := h.Auth.GenerateToken(key)
	if err != nil {
		return c.JSON(http.StatusUnauthorized, err.Error())
	}
	data := map[string]interface{}{
		"x-token": userAuth,
	}
	return c.JSON(http.StatusOK, data)
}
