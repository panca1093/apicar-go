package car

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

// Usecase - initiate usecase struct
type usecase struct {
	Repo Repository
}

// Usecase - Usecase interface
type Usecase interface {
	ProceedData(ctx context.Context, car *Car) error
	FindAll(ctx context.Context) ([]Car, error)
	FindbyFilter(ctx context.Context, filter Filter) ([]Car, error)
	DeleteData(ctx context.Context, id string) error
}

// NewUsecase - Function new usecase
func NewUsecase(repo Repository) Usecase {
	return &usecase{Repo: repo}
}

func (u *usecase) ProceedData(ctx context.Context, car *Car) error {
	if len(strings.TrimSpace(car.ID)) == 0 {
		car.ID = uuid.New().String()
	}
	return u.Repo.Store(ctx, car)
}

func (u *usecase) FindAll(ctx context.Context) ([]Car, error) {
	return u.Repo.FindAll(ctx)
}

func (u *usecase) FindbyFilter(ctx context.Context, filter Filter) ([]Car, error) {
	if filter.isEmpty() {
		return nil, nil
	}
	return u.Repo.FindbyFilter(ctx, filter)
}

func (u *usecase) DeleteData(ctx context.Context, id string) error {
	if len(strings.TrimSpace(id)) == 0 {
		return fmt.Errorf("id is empty")
	}
	return u.Repo.DeletebyID(ctx, id)
}
