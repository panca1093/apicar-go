package car

var cars []Car

// Car - struct data
type Car struct {
	ID            string  `json:"idCar,omitempty" bson:"idCar"`
	Name          string  `json:"name,omitempty" bson:"name"`
	Brand         string  `json:"brand,omitempty" bson:"brand"`
	Price         float64 `json:"price,omitempty" bson:"price"`
	Condition     string  `json:"condition,omitempty" bson:"condition"`
	Quantity      int     `json:"quantity,omitempty" bson:"quantity"`
	Specification string  `json:"specification,omitempty" bson:"specification"`
	Description   string  `json:"description,omitempty" bson:"description"`
	ImageURL      string  `json:"imageUrl,omitempty" bson:"imageUrl"`
}

// Filter - struct data filter
type Filter struct {
	ID   string `json:"idCar"`
	Name string `json:"name"`
}

// UserAuth - User Authentication model
type UserAuth struct {
	AccessToken string `json:"access_token,omitempty"`
	ExpireIn    int64  `json:"expires_in,omitempty"`
}

func (f *Filter) isEmpty() bool {
	if f.ID == "" && f.Name == "" {
		return true
	}
	return false
}
