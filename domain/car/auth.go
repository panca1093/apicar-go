package car

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/gommon/log"
)

type auth struct{}

// Auth - auth interface
type Auth interface {
	GenerateToken(secretKey string) (*UserAuth, error)
	ValidateSecretKey(key string) bool
}

// NewAuth - function new auth
func NewAuth() Auth {
	return &auth{}
}

var defaultExpireToken int64 = 10800

// GenerateToken - generate user access token
func (a *auth) GenerateToken(secretKey string) (*UserAuth, error) {
	valid := a.ValidateSecretKey(secretKey)
	if !valid {
		return nil, fmt.Errorf("Invalid secret key")
	}
	now := time.Now()
	issuedAt := now.Unix()
	expiresAt := now.Add(3 * time.Hour).Unix()
	claims := jwt.StandardClaims{
		ExpiresAt: expiresAt,
		Issuer:    "apicar-go",
		IssuedAt:  issuedAt,
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, &claims)
	token, err := jwtToken.SignedString([]byte(secretKey))
	if err != nil {
		log.Errorf("[ERROR] signing token")
		return nil, err
	}
	return &UserAuth{
		AccessToken: token,
		ExpireIn:    defaultExpireToken,
	}, nil
}

// ValidateSecretKey - Validate Secret Key
func (a *auth) ValidateSecretKey(key string) bool {
	if len(strings.TrimSpace(key)) == 0 {
		return false
	}
	secretKey := os.Getenv("TEST_SECRET_KEY")
	if len(strings.TrimSpace(secretKey)) == 0 {
		return false
	}
	if key != secretKey {
		return false
	}
	return true
}
