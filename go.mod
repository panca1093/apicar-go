module gitlab.com/panca1093/apicar-go

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	go.mongodb.org/mongo-driver v1.4.2
	google.golang.org/appengine v1.6.6
)
