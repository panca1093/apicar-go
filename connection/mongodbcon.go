package connection

import (
	"context"
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// NewMongoDBClient - Function Setup connection mongoDb
func NewMongoDBClient(uri string) (*mongo.Client, error) {
	if strings.TrimSpace(uri) == "" {
		return nil, fmt.Errorf("mongodb url is empty")
	}
	clientOption := options.Client().ApplyURI(uri)
	clien, err := mongo.Connect(context.Background(), clientOption)
	if err != nil {
		return nil, err
	}
	err = clien.Ping(context.Background(), nil)
	if err != nil {
		return nil, err
	}
	fmt.Println("[INFO] MongoDB is Connnected.")
	return clien, nil
}
